package com.example.nourbaha.sematecazar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nourbaha.sematecazar.Adapters.YahooWeatherAdapter;
import com.example.nourbaha.sematecazar.Models.YahooWeatherModel.YahooWeatherModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;

public class YahooWeatherActivity extends BaseActivity implements View.OnClickListener {
    EditText city;
    Button search;
    Button save;
    TextView result;
    TextView cityResult;
    TextView countryResult;
    ImageView image;
    TextView forecastListTitle;
    ListView forecastList;

    TextView battery;

//    ContentLoadingProgressBar progressBar;
    ProgressDialog progressDialog;
    YahooWeatherModel yahooWeather;
    BroadcastReceiver batteryStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yahoo_weather);
        getPermition();
        bindView();

        batteryStatus = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Integer level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                battery.setText("Battery : " + level + "" + " %");
            }
        };
        registerReceiver(batteryStatus, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    private void bindView() {
        city = (EditText) findViewById(R.id.city);
        search = (Button) findViewById(R.id.search);
        save = (Button) findViewById(R.id.save);
        result = (TextView) findViewById(R.id.result);
        cityResult = (TextView) findViewById(R.id.cityResult);
        countryResult = (TextView) findViewById(R.id.countryResult);
        forecastListTitle = (TextView) findViewById(R.id.forecastListTitle);
        image = (ImageView) findViewById(R.id.image);
        forecastList = (ListView) findViewById(R.id.forecastList);
        search.setOnClickListener(this);
        save.setOnClickListener(this);
        forecastListTitle.setVisibility(View.INVISIBLE);
        save.setVisibility(View.INVISIBLE);

        battery = (TextView) findViewById(R.id.battery);

        progressDialog = new ProgressDialog(mContext);
        progressDialog.setProgressStyle(ProgressDialog.BUTTON_POSITIVE);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.search) {
            String urlString = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                    city.getText() + "%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
            Log.d("weather_debuging", urlString);
            AsyncHttpClient client = new AsyncHttpClient();
            client.get(urlString, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    super.onStart();
                    progressDialog.show();

                }

                @Override
                public void onFinish() {
                    super.onFinish();
                    progressDialog.dismiss();

                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                    Log.d("weather_debuging", "Failure");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String responseString) {
                    Log.d("weather_debuging", "Success");
                    Log.d("weather_debuging", responseString);
                    Gson gson = new Gson();
                    yahooWeather =
                            gson.fromJson(responseString, YahooWeatherModel.class);
                    String temp = yahooWeather.getQuery().getResults()
                            .getChannel().getItem().getCondition().getTemp();
                    Log.d("weather_debuging", temp + " F");
                    result.setText(Utility.convertTempFtoC
                            (Integer.parseInt(temp)) + "" + "°");
                    cityResult.setText(yahooWeather.getQuery()
                            .getResults().getChannel().getLocation().getCity());
                    countryResult.setText(yahooWeather.getQuery()
                            .getResults().getChannel().getLocation().getCountry());
                    image.setImageResource(Utility.getWeatherIconId(
                            Integer.parseInt(yahooWeather.getQuery().getResults()
                                    .getChannel().getItem().getCondition().getCode())));

                    forecastListTitle.setVisibility(View.VISIBLE);

                    //Fill forecast List
                    YahooWeatherAdapter adapter =
                            new YahooWeatherAdapter(mContext, yahooWeather.getQuery()
                                    .getResults().getChannel().getItem().getForecast());
                    forecastList.setAdapter(adapter);
                    //yahooWeather.getQuery().save();
                }

            });
        } else if (view.getId() == R.id.save) {
            Log.d("weather_debuging", "Start Save");
            YahooWeatherModel yahooWeatherDB = new YahooWeatherModel(yahooWeather.getQuery());
//            yahooWeatherDB.save();
//            yahooWeatherDB.getQuery().getResults().save();
            yahooWeatherDB.getQuery().getResults().getChannel().save();
//            yahooWeatherDB.getQuery().getResults().getChannel().getAstronomy().save();
//            yahooWeatherDB.getQuery().getResults().getChannel().getAtmosphere().save();
//            yahooWeatherDB.getQuery().getResults().getChannel().getItem().save();
//            yahooWeatherDB.getQuery().getResults().getChannel().getItem().getCondition().save();
//            List<Forecast> forecasts =
//                    yahooWeatherDB.getQuery().getResults().getChannel().getItem().getForecast();
//            for (int i=0;i<forecasts.size();i++) {
//                forecasts.get(i).save();
//            }
//            yahooWeatherDB.getQuery().getResults().getChannel().getLocation().save();
//            yahooWeatherDB.getQuery().getResults().getChannel().getUnits().save();
//            yahooWeatherDB.getQuery().getResults().getChannel().getWind().save();
//            FoodModel food = new FoodModel();
//            food.setName("قرمه سبزی");
//            food.setPrice(12000);
//            food.setImageURL("Http://google.com");
//            food.save();
            Toast.makeText(mContext, "Save completely", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(batteryStatus);
    }

    public void getPermition() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this
                    ,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1500);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this
                    ,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1500);
        }

    }
}

