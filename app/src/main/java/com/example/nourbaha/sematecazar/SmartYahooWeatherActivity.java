package com.example.nourbaha.sematecazar;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.BatteryManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.nourbaha.sematecazar.Adapters.YahooWeatherAdapter;
import com.example.nourbaha.sematecazar.Models.YahooWeatherModel.YahooWeatherModel;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import cz.msebera.android.httpclient.Header;
import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class SmartYahooWeatherActivity extends BaseActivity implements View.OnClickListener {
    EditText city;
    Button search;
    Button gpsSearch;
    TextView result;
    TextView cityResult;
    TextView countryResult;
    ImageView image;
    TextView forecastListTitle;
    ListView forecastList;
    TextView battery;
    Double latitude;
    Double longitude;
    String url;
    ProgressDialog progressDialog;
    YahooWeatherModel yahooWeather;
    BroadcastReceiver batteryStatus;
    RelativeLayout citySearchLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yahoo_weather_smart);

        getPermition();
        bindView();
        checkGPSEnable();
        setBroadcast();
    }

    private void checkGPSEnable() {
        if(!SmartLocation.with(mContext).location().state().isGpsAvailable())
        {
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivityForResult(intent, 110);
        }
        else {
            getLocation();
        }
    }

    private void setBroadcast() {
        batteryStatus = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Integer level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                battery.setText("Battery : " + level + "" + " %");
            }
        };
        registerReceiver(batteryStatus, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
    }

    private void getLocation() {
        SmartLocation.with(mContext).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        url =
                                "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(SELECT%20woeid%20FROM%20geo.places%20WHERE%20text%3D%22(" +
                                        Double.toString(latitude) + "%2C" + Double.toString(longitude) + ")%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

                        getYahooWeather(url);
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 110)
        {
            if(SmartLocation.with(mContext).location().state().isGpsAvailable())
            {
                getLocation();
            }
            else {
                Utility.toast(mContext,"Your GPS is not available");
            }
        }
    }

    private void bindView() {
        city = (EditText) findViewById(R.id.city);
        search = (Button) findViewById(R.id.search);
        gpsSearch = (Button) findViewById(R.id.gpsSearch);
        result = (TextView) findViewById(R.id.result);
        cityResult = (TextView) findViewById(R.id.cityResult);
        countryResult = (TextView) findViewById(R.id.countryResult);
        forecastListTitle = (TextView) findViewById(R.id.forecastListTitle);
        image = (ImageView) findViewById(R.id.image);
        forecastList = (ListView) findViewById(R.id.forecastList);
        search.setOnClickListener(this);
        gpsSearch.setOnClickListener(this);
        forecastListTitle.setVisibility(View.INVISIBLE);
        battery = (TextView) findViewById(R.id.battery);
        progressDialog = new ProgressDialog(mContext);
        progressDialog.setProgressStyle(ProgressDialog.BUTTON_POSITIVE);
        citySearchLayout = (RelativeLayout) findViewById(R.id.citySearchLayout);
        citySearchLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.search) {
            url = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                    city.getText() + "%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

            getYahooWeather(url);
        } else if (view.getId() == R.id.gpsSearch)
        {
            checkGPSEnable();
        }
    }

    private void getYahooWeather(String url) {

        if (url.length() == 0)
            return;

        Log.d("weather_debuging", url);

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                progressDialog.show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                Log.d("weather_debuging", "Failure");
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                Log.d("weather_debuging", "Success");
                Log.d("weather_debuging", responseString);
                Gson gson = new Gson();
                yahooWeather =
                        gson.fromJson(responseString, YahooWeatherModel.class);
                String temp = yahooWeather.getQuery().getResults()
                        .getChannel().getItem().getCondition().getTemp();
                Log.d("weather_debuging", temp + " F");
                result.setText(Utility.convertTempFtoC
                        (Integer.parseInt(temp)) + "" + "°");
                cityResult.setText(yahooWeather.getQuery()
                        .getResults().getChannel().getLocation().getCity());
                countryResult.setText(yahooWeather.getQuery()
                        .getResults().getChannel().getLocation().getCountry());
                image.setImageResource(Utility.getWeatherIconId(
                        Integer.parseInt(yahooWeather.getQuery().getResults()
                                .getChannel().getItem().getCondition().getCode())));

                forecastListTitle.setVisibility(View.VISIBLE);

                //Fill forecast List
                YahooWeatherAdapter adapter =
                        new YahooWeatherAdapter(mContext, yahooWeather.getQuery()
                                .getResults().getChannel().getItem().getForecast());
                forecastList.setAdapter(adapter);
                //yahooWeather.getQuery().save();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(batteryStatus);
    }

    public void getPermition() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this
                    , new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 1500);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this
                    , new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1500);
        }

    }
}

