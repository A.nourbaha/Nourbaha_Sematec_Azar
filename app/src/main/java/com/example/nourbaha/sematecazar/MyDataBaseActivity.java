package com.example.nourbaha.sematecazar;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MyDataBaseActivity extends BaseActivity implements View.OnClickListener {
    EditText name;
    EditText family;
    Button save;
    Button show;
    TextView result;
    MyDataBaseHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_data_base);

        bindView();

        dbHandler =
                new MyDataBaseHandler(mContext, "myDatabase.db", null, 1);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this
                    ,new String[]{Manifest.permission.READ_PHONE_STATE}, 1500);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_NUMBERS) !=
                PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this
                    ,new String[]{Manifest.permission.READ_PHONE_NUMBERS}, 1500);
        }
    }

    private void bindView() {
        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        save = (Button) findViewById(R.id.save);
        show = (Button) findViewById(R.id.show);
        result = (TextView) findViewById(R.id.result);

        save.setOnClickListener(this);
        show.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.save: {

                dbHandler.insertStudent(name.getText().toString(), family.getText().toString());
                Toast.makeText(mContext, "New user have been added", Toast.LENGTH_SHORT).show();
                name.setText("");
                family.setText("");

                showResult();
                break;
            }
            case R.id.show: {
                showResult();
                break;
            }
        }
    }

    private void showResult() {
        result.setText(dbHandler.getStudents());
    }
}
