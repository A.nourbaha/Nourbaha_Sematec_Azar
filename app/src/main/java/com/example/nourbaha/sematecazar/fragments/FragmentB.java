package com.example.nourbaha.sematecazar.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.nourbaha.sematecazar.R;

/**
 * Created by Nourbaha on 12/30/2017.
 */

public class FragmentB extends Fragment {

    private static FragmentB frg;

    public static FragmentB getIns()
    {
        if (frg == null)
        {
            frg = new FragmentB();
        }
        return frg;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_b,container,false);

        return view;
    }
}
