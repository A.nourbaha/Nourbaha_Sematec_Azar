package com.example.nourbaha.sematecazar;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MeunActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meun);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(0,1,1,"Home");
        menu.add(0,10,10,"About");
        menu.add(0,100,100,"Setting");
        menu.add(0,1000,1000,"Exit");

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.base_menu,menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

//        switch (item.getItemId())
//        {
//            case 1 :
                Utility.toast(mContext,item.getTitle().toString());
//        }


        return super.onOptionsItemSelected(item);
    }
}
