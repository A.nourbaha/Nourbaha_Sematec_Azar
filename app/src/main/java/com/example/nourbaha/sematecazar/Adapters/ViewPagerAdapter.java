package com.example.nourbaha.sematecazar.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.nourbaha.sematecazar.fragments.FragmentA;
import com.example.nourbaha.sematecazar.fragments.FragmentB;
import com.example.nourbaha.sematecazar.fragments.FragmentC;


/**
 * Created by Nourbaha on 12/31/2017.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return FragmentA.getIns();
            case 1:
                return FragmentB.getIns();
            case 2:
                return FragmentC.getIns();
            default:
                return null;
        }
//        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Page A";
            case 1:
                return "Page B";
            case 2:
                return "Page C";
            default:
                return null;
        }
//        return super.getPageTitle(position);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
