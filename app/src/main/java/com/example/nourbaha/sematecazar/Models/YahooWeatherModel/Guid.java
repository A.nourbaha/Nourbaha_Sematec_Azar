
package com.example.nourbaha.sematecazar.Models.YahooWeatherModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class Guid extends SugarRecord<Guid> {

    @SerializedName("isPermaLink")
    @Expose
    private String isPermaLink;

    public String getIsPermaLink() {
        return isPermaLink;
    }

    public void setIsPermaLink(String isPermaLink) {
        this.isPermaLink = isPermaLink;
    }

}
