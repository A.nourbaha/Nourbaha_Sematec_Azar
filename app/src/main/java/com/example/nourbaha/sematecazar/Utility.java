package com.example.nourbaha.sematecazar;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

/**
 * Created by Nourbaha on 12/21/2017.
 */

public class Utility {
    public static Integer convertTempFtoC(Integer fTemp) {
        Double c;
        c = (fTemp - 32) / 1.8;

//        DecimalFormat df = new DecimalFormat("#");
//        Double.valueOf(df.format(c));
        return Integer.valueOf(c.intValue());
    }
    public static Integer getWeatherIconId(Integer code)
    {
        switch (code) {
            case 5:
            case 6:
            case 8:
            case 9:
            case 10:
            case 12:
            case 17:
            case 35:
            case 40:
                return R.drawable.weather_rainy;
            case 7:
            case 13:
            case 14:
            case 15:
            case 16:
            case 41:
            case 42:
            case 43:
                return R.drawable.weather_snowy;
            case 11:
                return R.drawable.weather_stormy;
            case 26:
                return R.drawable.weather_cloudy_2;
            case 27:
                return R.drawable.weather_night_cloudy_3;
            case 28:
                return R.drawable.weather_cloudy_3;
            case 29:
                return R.drawable.weather_night_cloudy;
            case 44:
            case 30:
                return R.drawable.weather_cloudy;
            case 31:
            case 33:
                return R.drawable.weather_night_clear;
            case 32:
            case 34:
                return R.drawable.weather_sunny;
            default:
                return R.drawable.weather_sunny;
        }

    }
    public static void toast(Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    public static void notification (Context context, String title, String content,
                                     String channelId , String channelName)
    {
        Intent intent = new Intent(context, MainActivity.class);
        PendingIntent pi = PendingIntent.getActivity(context,0,intent,0);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel channel =
                    new NotificationChannel
                            (channelId,channelName,NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription(channelName);
        notificationManager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(content)
                .setContentIntent(pi)
                .setAutoCancel(true);

        notificationManager.notify(0,builder.build());
    }
    public static void notification (Context context, String title, String content) {
        notification(context,title,content,"default","notification");
    }
}
