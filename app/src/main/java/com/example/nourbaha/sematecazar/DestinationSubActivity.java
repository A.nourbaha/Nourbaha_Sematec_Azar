package com.example.nourbaha.sematecazar;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DestinationSubActivity extends BaseActivity implements View.OnClickListener {

    EditText username;
    Button result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination_sub);

        username = (EditText) findViewById(R.id.txt_username);
        result = (Button) findViewById(R.id.submit);

        result.setOnClickListener(this);

        Intent intent = getIntent();

        username.setText(intent.getStringExtra("name"));

    }

    @Override
    public void onClick(View view) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result", username.getText().toString());
        setResult(Activity.RESULT_OK, returnIntent);
        finish();
    }
}
