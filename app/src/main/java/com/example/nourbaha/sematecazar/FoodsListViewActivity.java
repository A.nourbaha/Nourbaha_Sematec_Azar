package com.example.nourbaha.sematecazar;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.nourbaha.sematecazar.Adapters.FoodsListAdapter;
import com.example.nourbaha.sematecazar.Models.FoodModel;

import java.util.ArrayList;
import java.util.List;

public class FoodsListViewActivity extends BaseActivity {

    ListView foodList ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foods_list_view);

        foodList = (ListView) findViewById(R.id.foodsList);

        FoodModel gorme = new FoodModel();
        gorme.setName("قرمه سبزی");
        gorme.setPrice(12000);
        gorme.setImageURL("http://www.coca.ir/wp-content/uploads/2014/12/Ghormeh-Sabzi-2.jpg");

        FoodModel kabab = new FoodModel();
        kabab.setName("کباب کوبیده");
        kabab.setPrice(19000);
        kabab.setImageURL("http://parsino.com/wp-content/uploads/2017/09/%DA%A9%D8%A8%D8%A7%D8%A8-%DA%A9%D9%88%D8%A8%DB%8C%D8%AF%D9%87-1.jpg");

        FoodModel lobiapoolo = new FoodModel();
        lobiapoolo.setName("لوبیا پلو");
        lobiapoolo.setPrice(10000);
        lobiapoolo.setImageURL("http://salembeman.ir/wp-content/uploads/2016/12/666.jpg");

        FoodModel kofte = new FoodModel();
        kofte.setName("کوفته تبریزی");
        kofte.setPrice(16000);
        kofte.setImageURL("http://salamat.life/wp-content/uploads/2017/05/%D8%B7%D8%B1%D8%B2-%D8%AA%D9%87%DB%8C%D9%87-%DA%A9%D9%88%D9%81%D8%AA%D9%87-%D8%AA%D8%A8%D8%B1%DB%8C%D8%B2%DB%8C-%D8%B3%D8%A7%D8%AF%D9%87.jpg");

        List<FoodModel> foods = new ArrayList<>();

        foods.add(gorme);
        foods.add(kabab);
        foods.add(lobiapoolo);
        foods.add(kofte);

        FoodsListAdapter adapter = new FoodsListAdapter(mContext,foods);

        foodList.setAdapter(adapter);

        foodList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView name = view.findViewById(R.id.name);
                Toast.makeText(mContext, name.getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
