package com.example.nourbaha.sematecazar;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

public class WebViewActivity extends BaseActivity {
    Button show;
    EditText url;
    WebView webView;

    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        show = (Button) findViewById(R.id.show);
        url = (EditText) findViewById(R.id.url);
        webView = (WebView) findViewById(R.id.webView);

        dialog = new ProgressDialog(mContext);
        dialog.setTitle("ProgressDialog");
        dialog.setIcon(R.drawable.search_icon);
        dialog.setMessage("Please Wait");

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                webView.getSettings().setJavaScriptEnabled(true);
                webView.setWebViewClient(new MyWebViewClient());
                webView.loadUrl(url.getText().toString());

            }
        });
    }

    private class MyWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            dialog.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            dialog.dismiss();
        }
    }
}







