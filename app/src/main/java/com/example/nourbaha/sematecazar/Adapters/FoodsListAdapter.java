package com.example.nourbaha.sematecazar.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nourbaha.sematecazar.Models.FoodModel;
import com.example.nourbaha.sematecazar.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Nourbaha on 12/20/2017.
 */

public class FoodsListAdapter extends BaseAdapter {

    Context mContext;
    List<FoodModel> foods;

    public FoodsListAdapter(Context mContext, List<FoodModel> foods) {
        this.mContext = mContext;
        this.foods = foods;
    }

    @Override
    public int getCount() {
        return foods.size();
    }

    @Override
    public Object getItem(int position) {
        return foods.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View foodView =
                LayoutInflater.from(mContext)
                        .inflate(R.layout.food_item, viewGroup, false);

        TextView name = foodView.findViewById(R.id.name);
        TextView price = foodView.findViewById(R.id.price);
        ImageView image = foodView.findViewById(R.id.imageFood);

        //FoodModel food = foods.get(position);

        name.setText(foods.get(position).getName());
        price.setText(foods.get(position).getPrice() + "");

        Picasso.with(mContext).load(foods.get(position).getImageURL()).into(image);

        return foodView;
    }
}
