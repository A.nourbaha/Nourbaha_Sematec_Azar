package com.example.nourbaha.sematecazar;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    EditText username;
    EditText password;
    Button submit;
    Button showSecendActivity;
    Button showRelativeLayoutActivity;
    Button showScrollableActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindViews();


    }

    private void bindViews() {
        username = (EditText) findViewById(R.id.txt_username);
        password = (EditText) findViewById(R.id.txt_password);
        submit = (Button) findViewById(R.id.btn_submit);
        showSecendActivity = (Button) findViewById(R.id.btn_ShowSecendActivity);
        showRelativeLayoutActivity =
                (Button) findViewById(R.id.btn_ShowRelativeLayoutActivity);
        showScrollableActivity =
                (Button) findViewById(R.id.btn_ShowScrollableActivity);

        submit.setOnClickListener(this);
        showSecendActivity.setOnClickListener(this);
        showRelativeLayoutActivity.setOnClickListener(this);
        showScrollableActivity.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case (R.id.btn_submit) :
            {
                String strUsername = username.getText().toString();
                String strPassword = password.getText().toString();
                if (strUsername.length() == 0 || strPassword.length() == 0)
                {
                    Toast.makeText(mContext,"Please complete information",Toast.LENGTH_SHORT).show();
                    break;
                }
                username.setText("");
                password.setText("");
                Intent intent = new Intent(mContext, DestinationActivity.class);
                intent.putExtra("User_Pass", strUsername + " " + strPassword);
                startActivity(intent);
                break;
            }
            case (R.id.btn_ShowSecendActivity) :
            {
                Intent intent = new Intent(mContext, SecendActivity.class);
                startActivity(intent);
                break;
            }
            case (R.id.btn_ShowRelativeLayoutActivity) :
            {
                Intent intent = new Intent(mContext,RelativeLayoutActivity.class);
                startActivity(intent);
                break;
            }
            case (R.id.btn_ShowScrollableActivity) :
            {
                Intent intent = new Intent(mContext,ScrollableActivity.class);
                startActivity(intent);
            }
        }
    }
    Boolean backPress = false;
    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        if (!backPress)
        {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                  backPress = false;
                }
            },2000);

            Utility.toast(mContext,"Press back again to EXIT");
            backPress = true;
        } else finish();

    }
}
