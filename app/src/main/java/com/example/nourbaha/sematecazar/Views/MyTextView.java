package com.example.nourbaha.sematecazar.Views;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Nourbaha on 12/21/2017.
 */

public class MyTextView extends AppCompatTextView {
    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface tf_Regular = Typeface.createFromAsset(context.getAssets(),
                "DiodrumArabic-Regular.ttf");
        Typeface tf_Medium = Typeface.createFromAsset(context.getAssets(),
                "DiodrumArabic-Medium.ttf");
        this.setTypeface(tf_Medium);
    }
}
