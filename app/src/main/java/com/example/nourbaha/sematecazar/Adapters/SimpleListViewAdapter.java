package com.example.nourbaha.sematecazar.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.nourbaha.sematecazar.R;

/**
 * Created by Nourbaha on 12/20/2017.
 */

public class SimpleListViewAdapter extends BaseAdapter {
    Context mContext;
    String items[];

    public SimpleListViewAdapter(Context mContext, String items[]) {
        this.mContext = mContext;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int position) {
        return items[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView =
                LayoutInflater.from(mContext)
                        .inflate(R.layout.simple_list_item, viewGroup, false);
        TextView item = (TextView)
                rowView.findViewById(R.id.list_item);

        item.setText(items[position].toString());

        return rowView;
    }
}
