package com.example.nourbaha.sematecazar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class DestinationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);

        TextView result = (TextView) findViewById(R.id.result);
        Intent intent = getIntent();
        String strResult = intent.getStringExtra("User_Pass");
        result.setText(strResult);
        Toast.makeText(mContext,strResult,Toast.LENGTH_SHORT).show();

    }
}
