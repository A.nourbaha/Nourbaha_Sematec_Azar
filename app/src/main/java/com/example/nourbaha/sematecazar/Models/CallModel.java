package com.example.nourbaha.sematecazar.Models;

import com.orm.SugarRecord;

/**
 * Created by Nourbaha on 12/26/2017.
 */

public class CallModel extends SugarRecord<CallModel> {
    String number;
    String displayName;
    String timesContacted;
    String type;
    String displayNameSource;
    String timesUsed;

    public CallModel() {
    }

    public CallModel(String number, String displayName, String timesContacted, String type, String displayNameSource, String timesUsed) {
        this.number = number;
        this.displayName = displayName;
        this.timesContacted = timesContacted;
        this.type = type;
        this.displayNameSource = displayNameSource;
        this.timesUsed = timesUsed;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getTimesContacted() {
        return timesContacted;
    }

    public void setTimesContacted(String timesContacted) {
        this.timesContacted = timesContacted;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDisplayNameSource() {
        return displayNameSource;
    }

    public void setDisplayNameSource(String displayNameSource) {
        this.displayNameSource = displayNameSource;
    }

    public String getTimesUsed() {
        return timesUsed;
    }

    public void setTimesUsed(String timesUsed) {
        this.timesUsed = timesUsed;
    }
}
