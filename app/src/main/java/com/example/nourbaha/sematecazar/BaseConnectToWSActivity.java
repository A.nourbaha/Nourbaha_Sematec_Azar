package com.example.nourbaha.sematecazar;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class BaseConnectToWSActivity extends AppCompatActivity {
    EditText city;
    Button search;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base_connect_to_ws);

        city = (EditText) findViewById(R.id.city);
        search = (Button) findViewById(R.id.search);
        result = (TextView) findViewById(R.id.result);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String urlString = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22" +
                                city.getText() + "%2C%20ak%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
                        Log.d("weather_debuging", urlString);

                        String resposne = connectToWeatherWS(urlString);
                        Integer tempInt = getTemp(resposne);
                        Log.d("weather_debuging", tempInt+""+ " °F");
                        final String tempStr =
                                city.getText() + " : " +
                                        Utility.convertTempFtoC(tempInt) + "" + " °C";
                        Log.d("weather_debuging", tempStr);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                result.setText(tempStr);
                            }
                        });
                    }
                }).start();
            }
        });
    }

    Integer getTemp(String response) {
        Integer temp = 0;
        try {
            JSONObject responseObj = new JSONObject(response);

            String queryStr = responseObj.getString("query");
            JSONObject queryObj = new JSONObject(queryStr);

            String resultsStr = queryObj.getString("results");
            JSONObject resultsObj = new JSONObject(resultsStr);

            String channelStr = resultsObj.getString("channel");
            JSONObject channelObj = new JSONObject(channelStr);

            String itemStr = channelObj.getString("item");
            JSONObject itemObj = new JSONObject(itemStr);

            String conditionStr = itemObj.getString("condition");
            JSONObject conditionObj = new JSONObject(conditionStr);

            String tempStr = conditionObj.getString("temp");

            temp = Integer.parseInt(tempStr);

//            Log.d("weather_debuging", temp + "");

        } catch (Exception e) {
            Log.d("weather_debuging", e.getMessage());
        }
        return temp;
    }

    String connectToWeatherWS(String url) {
        String result = "";
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            con.setRequestMethod("GET");

            Log.d("weather_debuging", con.getResponseCode() + "");

            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    Log.d("weather_debuging", inputLine);
                    response.append(inputLine);
                }
                result = response.toString();
            }
        } catch (Exception e) {
            Log.d("weather_debuging", e.getMessage());
        }
        return result;
    }
}
