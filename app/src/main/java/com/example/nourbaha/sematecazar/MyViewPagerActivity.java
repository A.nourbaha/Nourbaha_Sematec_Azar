package com.example.nourbaha.sematecazar;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.astuetz.PagerSlidingTabStrip;
import com.example.nourbaha.sematecazar.Adapters.ViewPagerAdapter;
import com.example.nourbaha.sematecazar.BaseActivity;
import com.example.nourbaha.sematecazar.R;

public class MyViewPagerActivity extends BaseActivity {
    ViewPager pager;
    PagerSlidingTabStrip tabStrip ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_view_pager);

        pager = (ViewPager) findViewById(R.id.pager);
        tabStrip = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        ViewPagerAdapter fpa = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(fpa);
        tabStrip.setViewPager(pager);


    }
}
