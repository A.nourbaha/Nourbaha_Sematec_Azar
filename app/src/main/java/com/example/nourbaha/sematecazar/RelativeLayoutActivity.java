package com.example.nourbaha.sematecazar;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class RelativeLayoutActivity extends BaseActivity implements View.OnClickListener {

    Button add;
    TextView show1;
    TextView show2;
    TextView show3;
    TextView show21;
    TextView show22;
    TextView show23;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relative_layout);

        bindView();
    }

    private void bindView() {
        add = (Button) findViewById(R.id.txtView_Add);
        show1 = (TextView) findViewById(R.id.txtView_show1);
        show2 = (TextView) findViewById(R.id.txtView_show2);
        show3 = (TextView) findViewById(R.id.txtView_show3);
        show21 = (TextView) findViewById(R.id.txtView_show21);
        show22 = (TextView) findViewById(R.id.txtView_show22);
        show23 = (TextView) findViewById(R.id.txtView_show23);

        add.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (show1.getText().length() == 0) {
            show1.setText("Add 1");
        } else if (show2.getText().length() == 0) {
            show2.setText("Add 2");
        } else if (show3.getText().length() == 0) {
            show3.setText("Add 3");
        } else if (show21.getText().length() == 0) {
            show21.setText("Add 4");
        } else if (show22.getText().length() == 0) {
            show22.setText("Add 5");
        } else if (show23.getText().length() == 0) {
            show23.setText("Add 6");
        }
    }
}
