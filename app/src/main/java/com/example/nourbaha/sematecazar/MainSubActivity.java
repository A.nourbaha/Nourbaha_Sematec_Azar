package com.example.nourbaha.sematecazar;

import android.app.Activity;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainSubActivity extends BaseActivity implements View.OnClickListener {

    Button submit;
    Button gotoGPS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_sub);
        submit = (Button) findViewById(R.id.btn_submit);
        gotoGPS = (Button) findViewById(R.id.gotoGPS);
        submit.setOnClickListener(this);
        gotoGPS.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            //return from subActivity
            case 100: {
                if (resultCode == Activity.RESULT_OK) {
                    String result = data.getStringExtra("result");
                    Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();
                    break;
                }
            }
            //return from gps
            case 110: {
//                if (resultCode == Activity.RESULT_OK)
                    Toast.makeText(mContext, "Thank you for set GPS", Toast.LENGTH_SHORT).show();
                break;
            }
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit: {
                Intent intent = new Intent(mContext, DestinationSubActivity.class);
                intent.putExtra("name", "Vahid Nourbaha");
                startActivityForResult(intent, 100);
                break;
            }
            case R.id.gotoGPS: {
//                Intent intent = new Intent("android.location.GPS_ENABLED_CHANGE");
//                intent.putExtra("enabled",true);
//                sendBroadcast(intent);

                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 110);
                break;
            }

        }
    }
}
