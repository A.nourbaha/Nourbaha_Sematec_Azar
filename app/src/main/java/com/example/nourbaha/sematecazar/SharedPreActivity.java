package com.example.nourbaha.sematecazar;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

//import com.orhanobut.hawk.Hawk;

public class SharedPreActivity extends BaseActivity {

    EditText name;
    EditText family;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_pre);

//        Hawk.init(mContext).build();

        name = (EditText) findViewById(R.id.name);
        family = (EditText) findViewById(R.id.family);
        submit = (Button) findViewById(R.id.submit);

        name.setText(getShared("name", ""));
        family.setText(getShared("family", ""));

//        name.setText(getHwak("name", ""));
//        family.setText(getHwak("family", ""));

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                setShared("name", name.getText().toString());
                setShared("family", family.getText().toString());

//                setHwak("name", name.getText().toString());
//                setHwak("family", family.getText().toString());

                Toast.makeText(mContext, "Save Data", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    void setHwak(String key, String value) {
//        Hawk.put(key, value);
//    }
//
//    String getHwak(String key, String defaultValue) {
//        return Hawk.get(key,defaultValue);
//    }

    void setShared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString(key, value).commit();

    }

    String getShared(String key, String defaultValue) {
        return PreferenceManager.getDefaultSharedPreferences(this)
                .getString(key, defaultValue);
    }
}
