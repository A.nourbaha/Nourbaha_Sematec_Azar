package com.example.nourbaha.sematecazar.Models;

/**
 * Created by Nourbaha on 12/20/2017.
 */

public class FoodModel {

    String name;
    Integer price;
    String imageURL;

    public FoodModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }
}
