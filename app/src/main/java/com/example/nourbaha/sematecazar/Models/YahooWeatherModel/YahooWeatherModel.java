
package com.example.nourbaha.sematecazar.Models.YahooWeatherModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.orm.SugarRecord;

public class YahooWeatherModel extends SugarRecord<YahooWeatherModel>{

    @SerializedName("query")
    @Expose
    private Query query;

    public YahooWeatherModel() {
    }

    public YahooWeatherModel(Query query) {
        this.query = query;
    }

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

}
