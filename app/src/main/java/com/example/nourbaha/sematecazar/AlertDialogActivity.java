package com.example.nourbaha.sematecazar;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;

public class AlertDialogActivity extends BaseActivity implements View.OnClickListener {
    Button showDialog1;
    Button showDialog2;
    Button showDialog3;
    Button showDialog4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alert_dialog);

        showDialog1 = (Button) findViewById(R.id.showDialog1);
        showDialog2 = (Button) findViewById(R.id.showDialog2);
        showDialog3 = (Button) findViewById(R.id.showDialog3);
        showDialog4 = (Button) findViewById(R.id.showDialog4);

        showDialog1.setOnClickListener(this);
        showDialog2.setOnClickListener(this);
        showDialog3.setOnClickListener(this);
        showDialog4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.showDialog1: {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Alert1");
                alertDialog.setMessage("Test dialog alert. Are you sure?");
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "BUTTON_POSITIVE",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                alertDialog.show();
                break;
            }
            case R.id.showDialog2: {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Alert2");
                alertDialog.setMessage("Test dialog alert. Are you sure?");
                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "BUTTON_NEGATIVE",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                alertDialog.show();
                break;
            }
            case R.id.showDialog3: {
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Alert1");
                alertDialog.setMessage("Test dialog alert. Are you sure?");
                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "BUTTON_NEUTRAL",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                alertDialog.show();
                break;
            }
            case R.id.showDialog4: {
                Intent intent = new Intent(mContext,AlertActivity.class);
                startActivity(intent);
//                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
//                alertDialog.setTitle("Alert1");
//                alertDialog.setMessage("Test dialog alert. Are you sure?");
//                alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL, "BUTTON_NEUTRAL",
//                        new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialogInterface, int i) {
//                                dialogInterface.dismiss();
//                            }
//                        });
//                alertDialog.show();
                break;
            }
        }
    }
}
