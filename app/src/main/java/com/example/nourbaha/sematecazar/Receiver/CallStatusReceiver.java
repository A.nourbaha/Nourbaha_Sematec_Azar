package com.example.nourbaha.sematecazar.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.example.nourbaha.sematecazar.Models.CallModel;

/**
 * Created by Nourbaha on 12/26/2017.
 */

public class CallStatusReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {


        Toast.makeText(context,
                intent.getStringExtra(TelephonyManager.EXTRA_STATE), Toast.LENGTH_SHORT).show();


        // TODO Auto-generated method stub
        Bundle bundle = intent.getExtras();

        if (bundle == null)
            return;

//        SharedPreferences sp = context.getSharedPreferences("ZnSoftech", Activity.MODE_PRIVATE);

        String state = bundle.getString(TelephonyManager.EXTRA_STATE);

//        if (intent.getAction().equals(Intent.ACTION_NEW_OUTGOING_CALL)) {
//            String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
//            sp.edit().putString("number", number).commit();
//            sp.edit().putString("state", state).commit();
//        } else if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
//            String number = bundle.getString("incoming_number");
//            sp.edit().putString("number", number).commit();
//            sp.edit().putString("state", state).commit();
//        } else if (state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK)) {
//            sp.edit().putString("state", state).commit();
//        } else if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
//            String state = sp.getString("state", null);
//            if (!state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
//                sp.edit().putString("state", null).commit();
//                History h = new History(new Handler(), context);
//                context.getContentResolver().registerContentObserver(CallLog.Calls.CONTENT_URI, true, h);
//            }
//            sp.edit().putString("state", state).commit();
//        }




    String NUMBER = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
    String DISPLAY_NAME = intent.getStringExtra(Intent.ACTION_DATE_CHANGED);
    String TIMES_CONTACTED = intent.getStringExtra(Intent.ACTION_TIME_CHANGED);// ContactsContract.CommonDataKinds.Phone.TIMES_CONTACTED);
    String TYPE = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
//    String DISPLAY_NAME_SOURCE = intent.getStringExtra(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME_SOURCE);
//    String TIMES_USED = intent.getStringExtra(ContactsContract.CommonDataKinds.Phone.TIMES_USED);

    CallModel callModel = new CallModel();
        callModel.setNumber(NUMBER);
        callModel.setDisplayName(DISPLAY_NAME);
        callModel.setTimesContacted(TIMES_CONTACTED);
        callModel.setType(TYPE);
//        callModel.setDisplayNameSource(DISPLAY_NAME_SOURCE);
//        callModel.setTimesUsed(TIMES_USED);
        callModel.save();


}
}
