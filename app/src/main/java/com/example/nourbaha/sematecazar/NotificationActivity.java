package com.example.nourbaha.sematecazar;

import android.os.Bundle;
import android.view.View;

public class NotificationActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        findViewById(R.id.show).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.notification(mContext,"Test","Show notification for open main Activity");
            }
        });
    }
}
