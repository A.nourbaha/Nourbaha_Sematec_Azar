package com.example.nourbaha.sematecazar.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.nourbaha.sematecazar.Models.YahooWeatherModel.Forecast;
import com.example.nourbaha.sematecazar.R;
import com.example.nourbaha.sematecazar.Utility;

import java.util.List;

/**
 * Created by Nourbaha on 12/22/2017.
 */

public class YahooWeatherAdapter extends BaseAdapter {
    Context mContext;
    List<Forecast> forecasts;

    public YahooWeatherAdapter(Context mContext, List<Forecast> forecasts) {
        this.mContext = mContext;
        this.forecasts = forecasts;
    }

    @Override
    public int getCount() {
        return forecasts.size();
    }

    @Override
    public Object getItem(int position) {
        return forecasts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View weatherItem = LayoutInflater.from(mContext)
                .inflate(R.layout.weather_forecast_item, viewGroup, false);

        TextView date = weatherItem.findViewById(R.id.date);
        TextView day = weatherItem.findViewById(R.id.day);
        TextView minTemp = weatherItem.findViewById(R.id.minTemp);
        TextView maxTemp = weatherItem.findViewById(R.id.maxTemp);
        TextView type = weatherItem.findViewById(R.id.type);
        ImageView img = weatherItem.findViewById(R.id.image);

        date.setText("Date : " + forecasts.get(position).getDate());

        switch (forecasts.get(position).getDay().toLowerCase()) {
            case "sat": {
                day.setText("Saturday");
                break;
            }
            case "sun": {
                day.setText("Sunday");
                break;
            }
            case "mon": {
                day.setText("Monday");
                break;
            }
            case "tue": {
                day.setText("Tuesday");
                break;
            }
            case "wed": {
                day.setText("Wednesday");
                break;
            }
            case "thu": {
                day.setText("Thursday");
                break;
            }
            case "fri": {
                day.setText("Friday");
                break;
            }
        }

        minTemp.setText("Low : " +
                Utility.convertTempFtoC
                        (Integer.parseInt(forecasts.get(position).getLow())) + " °C");
        maxTemp.setText("High : " +
                Utility.convertTempFtoC
                        (Integer.parseInt(forecasts.get(position).getHigh())) + " °C");
        type.setText(forecasts.get(position).getText());

        Integer code = Integer.parseInt(forecasts.get(position).getCode());
        img.setImageResource(Utility.getWeatherIconId(code));
//        switch (code) {
//            case 5:
//            case 6:
//            case 8:
//            case 9:
//            case 10:
//            case 17:
//            case 35:
//            case 40:
//                img.setImageResource(R.drawable.weather_rainy);
//                break;
//            case 7:
//            case 13:
//            case 14:
//            case 15:
//            case 16:
//            case 41:
//            case 42:
//            case 43:
//                img.setImageResource(R.drawable.weather_snowy);
//                break;
//            case 11:
//            case 12:
//                img.setImageResource(R.drawable.weather_stormy);
//                break;
//            case 26:
//                img.setImageResource(R.drawable.weather_cloudy_2);
//                break;
//            case 27:
//                img.setImageResource(R.drawable.weather_night_cloudy_3);
//                break;
//            case 28:
//                img.setImageResource(R.drawable.weather_cloudy_3);
//                break;
//            case 29:
//                img.setImageResource(R.drawable.weather_night_cloudy);
//                break;
//            case 44:
//            case 30:
//                img.setImageResource(R.drawable.weather_cloudy);
//                break;
//            case 31:
//            case 33:
//                img.setImageResource(R.drawable.weather_night_clear);
//                break;
//            case 32:
//            case 34:
//                img.setImageResource(R.drawable.weather_sunny);
//                break;
//            default:
//                img.setImageResource(R.drawable.weather_sunny);
//                break;
//
//        }


        return weatherItem;
    }
}
