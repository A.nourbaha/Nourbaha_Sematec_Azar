package com.example.nourbaha.sematecazar.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.nourbaha.sematecazar.Utility;
import com.example.nourbaha.sematecazar.YahooWeatherActivity;

/**
 * Created by Nourbaha on 12/26/2017.
 */

public class ChangeStatusBatryReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        switch (intent.getAction()) {
            case Intent.ACTION_POWER_CONNECTED:
            {
                intent = new Intent(context, YahooWeatherActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                break;
            }
            case Intent.ACTION_POWER_DISCONNECTED :
            {
                Utility.toast(context, "Power Disconnected");
                break;
            }
        }
    }
}
