package com.example.nourbaha.sematecazar;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Nourbaha on 12/24/2017.
 */

public class MyDataBaseHandler extends SQLiteOpenHelper {

    String sqlQuery = "" +
            "CREATE TABLE students ( " +
            " _id INTEGER PRIMARY KEY AUTOINCREMENT , " +
            " name TEXT , " +
            " family TEXT )";

    public MyDataBaseHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.d("MydatabseHandler", sqlQuery);
        sqLiteDatabase.execSQL(sqlQuery);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void insertStudent(String name, String family) {
        String query = "" +
                "INSERT INTO students ( name , family ) " +
                "VALUES ( '" + name + "', " +
                "'" + family + "' )";
        Log.d("MydatabseHandler", query);
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(query);
        db.close();
    }

    public String getStudents() {
        String resutl = "";
        String query =
                "SELECT name, family FROM students";

        SQLiteDatabase db = this.getReadableDatabase();
        Log.d("MydatabseHandler", query);
        Cursor cursor = db.rawQuery(query, null);
        while (cursor.moveToNext()) {
            resutl += cursor.getString(0) + " ";
            resutl += cursor.getString(1) + "\n";
        }
        db.close();
        return resutl;
    }
}
