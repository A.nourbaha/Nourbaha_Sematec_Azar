package com.example.nourbaha.sematecazar;

import android.os.Bundle;
import android.widget.ListView;

import com.example.nourbaha.sematecazar.Adapters.SimpleListViewAdapter;

public class SimpleListviewActivity extends BaseActivity {

    ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_listview);

        listView = (ListView) findViewById(R.id.simple_list_view);

        String names[] = {
                "Alireza" ,
                "Alireza" ,
                "Mahmood" ,
                "Mahmood" ,
                "Mahmood" ,
                "Hasan" ,
                "Hasan" ,
                "Maryam" ,
                "Maryam" ,
                "Akbar",
                "Akbar"
        };

        SimpleListViewAdapter adapter = new SimpleListViewAdapter(mContext,names);

        listView.setAdapter(adapter);


    }
}
